import hashlib
import json
import os

def createStr(s):
    chars = []
    chars.extend(['+', '-', '_'])
    for i in range(ord('0'), ord('9') + 1):
        chars.append(chr(i))
    for i in range(ord('a'), ord('z') + 1):
        chars.append(chr(i))
    for i in range(ord('A'), ord('Z') + 1):
        chars.append(chr(i))

    m = hashlib.md5()
    m.update(s.encode())

    rslt = [chars[x % len(chars)] for x in m.digest()]
    s = ""
    for i in rslt:
        s += i
    # the first letter is one of +-_
    # then the output must contain at least one signature.
    firstChar = chars[m.digest()[0] % 3]
    return firstChar + s

def main():
    dic = json.loads(open("dic.txt", "r").read())

    print("# input your service.")
    service = input()
    if not service in dic.keys():
        print("##### not found the service. #####")
        return

    print("# input your account.")
    account = input()
    if not account in dic[service]:
        print("##### not found the account in the service. #####")
    
    print("enter your password.")
    password = input()
    md5 = hashlib.md5()
    md5.update(password.encode())
    
    filename = "hash.txt"
    if os.path.isfile(filename):
        f = open(filename, "r")
        if md5.hexdigest() != f.read():
            print("##### unknown password? #####")
            return
    else:
        f = open(filename, "w")
        f.write(md5.hexdigest())
    
    s = createStr(service + account + password)
    print(s)

if __name__ == '__main__':
    main()
    print("fin...")
    input()

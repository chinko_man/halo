import hashlib
import json
import logging

logger = logging.getLogger()
logging.basicConfig(level=logging.INFO)
logging = None


def myprint(s):
    outstr = ""
    for i in range(len(s)):
        if i % 5 == 0 and i != 0:
            outstr += " "
        outstr += s[i]
    print(outstr)

def strengthen(s):
    chars = []
    chars.extend(['+', '-', '_'])
    for i in range(ord('0'), ord('9') + 1):
        chars.append(chr(i))
    for i in range(ord('a'), ord('z') + 1):
        chars.append(chr(i))
    for i in range(ord('A'), ord('Z') + 1):
        chars.append(chr(i))
    logger.debug(chars)

    m = hashlib.md5()
    m.update(s.encode())
    
    logger.debug(m.digest())
    rslt = [chars[x % len(chars)] for x in m.digest()]
    logger.debug(rslt)

    s = ""
    for i in rslt:
        s += i
    return s

def create_str(d):
    f = open("dict.txt")
    words = json.loads(f.read())
    f.close()
    logger.debug(words)

    print("# input a serviceName")
    serviceName = input()
    ret = serviceName
    if not serviceName in words:
        print("##############################################\n")
        print("I don't know the name. is it correct?(q...quit)\n")
        print("##############################################\n")
        if input() == "q":
            exit()
    else:
        print("I know the word")

    print("# input your account name")
    accountName = input()
    ret += accountName
    if not accountName in words:
        print("##############################################\n")
        print("I don't know the name. is it correct?(q...quit)\n")
        print("##############################################\n")
        if input() == "q":
            exit()
    else:
        print("I know the word")

    # account name and service name in lower case. so as not to be confused.
    ret = ret.lower()

    ret += d["oldPassword"]
    ret += str(d["year"])
    ret += str(d["month"])
    ret += str(d["day"])
    ret += d["pianist"]
    ret += d["composer"]
    print("# input your password")
    password = input()
    ret += password
    logger.debug(ret)

    processHash(d, password)

    return ret


def processHash(d, password):
    strForHash  = d["oldPassword"]
    strForHash += str(d["year"])
    strForHash += str(d["month"])
    strForHash += str(d["day"])
    strForHash += d["pianist"]
    strForHash += d["composer"]
    strForHash += password
    sha = hashlib.sha256(strForHash.encode())

    data = ""
    try:
        f = open("history.txt", "r")
        data = f.read()
        f.close()
    except:
        print("######################################################################\n")
        print("no history.txt, create one?(y/n)\n")
        print("######################################################################\n")
        if input() == "y":
            f = open("history.txt", "w")
            f.write(sha.hexdigest())
            f.close()
        else:
            exit()

    if sha.hexdigest() != data:
        print("#######################################################################\n")
        print("you have not input a password you used before!")
        print("that means it may be a wrong password or a wrong history file. OK?\n")
        print("(q...quit)")
        print("#######################################################################\n")
        if input() == "q":
            exit()
    else:
        print("### OK! I know your password and you.")
    

def main():
    print("######################################################################\n")
    print("This app needs 2 or 3 txt files for checking if your inputs are valid.")
    print("    data.txt, personal data is stored.")
    print("    dict.txt, prevents from miss-typing. edit by yourself.")
    print("    history.txt, created from your personal data and your password.")
    print("    if no history.txt, this app can create a history.txt.\n")
    print("    Note that if the created password doesn't have any signatures,")
    print("    you may add a plus symbol at the top of the password.")
    print("    So, a correct password is the created str or '+' + created_str.")
    print("######################################################################\n")
    f = open("data.txt")
    data = f.read()
    f.close()

    s = create_str(d = json.loads(data))
    rslt = strengthen(s)
    print("<" + rslt + ">")
    myprint(rslt)
    
    print("")
    for i in range(3):
        rslt = strengthen(rslt)
        print("<" + rslt + ">")
        myprint(rslt)

    print("finished...")
    input()

def testMain():
    print("# enter a str, which is crypted and displayed.")
    print("service, account, oldPass, year, month, day, pianist, composer, password")
    print(">>>", end="")
    s = input()
    rslt = strengthen(s)
    print(rslt)
    for i in range(3):
        rslt = strengthen(rslt)
        print("OMAKE:" + rslt)
    print("finished...")
    input()

if __name__ == '__main__':
    print("test mode? (y/n)")
    cmd = input()
    if cmd == "n":
        main()
    elif cmd == "y":
        testMain()
    else:
        print("invalud cmd")
        input()


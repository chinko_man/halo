stubstubstubstubstub



def convertToAlphabet(s):
    ret = ""
    num = 26
    for i in s:
        ret += chr(ord("A") + ord(i) % num)

    #print(num, ret)
    return ret

def overlayStr(strA, strB):
    strA = convertToAlphabet(strA)
    strB = convertToAlphabet(strB)

    if len(strA) > len(strB):
        strB = getWrappedStr(strB, len(strA))
    elif len(strA) < len(strB):
        strA = getWrappedStr(strA, len(strB))
    #print(strA, strB)
    ret = ""
    for i in range(len(strA)):
        num = ord("A") + (ord(strA[i]) + ord(strB[i])) % 26
        ret += chr(num)

    return ret

def getWrappedStr(s, length):
    if len(s) == 0:
        print("ERROR")
        raise Exception("gyaaaa")
    
    times = length // len(s)
    mod = length % len(s)
    return s * times + s[:mod]



def myCreatePassword(
    serviceUrlName : str, 
    accountName    : str,
    passwordInUniv : str, 
    yourBirthYear  : int, 
    yourBirthMonth : int, 
    yourBirthDay   : int, 
    nameOfPianist  : str, 
    composer       : str,
    password       : str):
    """
    If this generator is stolen by someone, 
    there are too many keys to get a correct password.
    """
    tmp = getWrappedStr(serviceUrlName.lower(), 12)
    tmp = overlayStr(tmp, accountName.lower())
    tmp = overlayStr(tmp, passwordInUniv.lower())
    tmp = overlayStr(tmp, nameOfPianist.lower())
    tmp = overlayStr(tmp, composer.lower())
    
    tmp = chr(ord("a") + yourBirthYear % 26) + tmp
    tmp = chr(ord("a") + yourBirthMonth % 26) + tmp
    tmp = chr(ord("a") + yourBirthDay % 26) + tmp
    
    tmptmp = ""
    for i in tmp.lower():
        code = ord("A") + (ord(i) + yourBirthMonth) % 26
        tmptmp += chr(code)
    
    tmpRslt = str(yourBirthDay) + tmp + "_" + tmptmp
    
    return "+-_" + decorateWithPassword(tmpRslt, password)


def decorateWithNum(s, num):
    ret = ""
    for i in range(len(s)):
        if ord(s[i]) >= ord("a") and ord(s[i]) <= ord("z"):
            ret += chr(ord("a") + (ord(s[i]) + num) % 26)
        elif ord(s[i]) >= ord("A") and ord(s[i]) <= ord("Z"):
            ret += chr(ord("A") + (ord(s[i]) + num) % 26)
        else:
            ret += s[i]
    return ret

def decorateWithPassword(s, password):
    tmpPass = getWrappedStr(password, len(s))
    charArr = []
    for i in range(ord("1"), ord("9") + 1):
        charArr.append(chr(i))
    for i in range(ord("a"), ord("z") + 1):
        charArr.append(chr(i))
    for i in range(ord("A"), ord("Z") + 1):
        charArr.append(chr(i))
    charArr.extend(["+", "-", "_"])
    
    ret = ""
    for i in range(len(s)):
        num = len(charArr)
        idx = (ord(s[i]) + ord(tmpPass[i])) % num
        ret += charArr[idx]
    
    return ret

def myUtilRun(serviceName, password, accountName):
    import myData
    return myCreatePassword(serviceName, accountName, myData.myInfo["passInUniv"], myData.myInfo["year"], myData.myInfo["month"], myData.myInfo["day"], myData.myInfo["pianist"], myData.myInfo["composer"], password)
    


if __name__ == '__main__':
    print("# entered strings are converted with lower() method.")
    print("enter the serviceUrlName")
    a = input()
    print("enter your accountName")
    b = input()
    print("enter your passwordInUniversity")
    c = input()
    print("your birth year")
    d = input()
    print("your birth month")
    e = input()
    print("your birth day")
    f = input()
    d = int(d)
    e = int(e)
    f = int(f)
    print("name of a pianist")
    g = input()
    print("name of composer")
    h = input()
    print("enter your password")
    i = input()

    print("\n\n\nyour password is ...")
    rslt = myCreatePassword(a, b, c, d, e, f, g, h, i)
    print(rslt)
    
    input()
    

